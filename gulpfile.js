{
  /*
  ** Dependencias
  */
  let gulp = require('gulp');
  let uglify = require('gulp-uglify');

  /*
  * Configuración de la tarea 'default'
  */
  gulp.task('default', () => {
    gulp.src('server.js')
    .pipe(uglify())
    .pipe(gulp.dest('index.js'));
  });
}
