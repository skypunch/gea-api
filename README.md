# GEA
## Especificaciones HTTP

### Métodos

| Método | CRUD | MongoDB |
|:-:|:-:|:-:|
| __POST__| _CREATE_ | ___INSERT___ |
| __GET__ | _READ_ | ___FIND___ |
| __PUT__| _UPDATE_ | ___UPDATE___ |
| __DETELE__ | _DELETE_ | ___REMOVE___ |

### Códigos de Estado

Códigos de Estado implementado en el API

| Tipo | Codigo | Significado |
| :-: | :-: | :-: |
| __SUCCESS__ | ___200___ | __OK__: _Solicitud Correcta_ |
| __SUCCESS__ | ___201___ | __CREATED__: _Entidad Creada_ |
| __SUCCESS__ | ___204___ | __NO CONTENT__: _Sin contenido (entidades)_ |
| __ERRORS__ | ___400___ | __BAD REQUEST__: _Error en la Solicitud_ |
| __ERRORS__ | ___401___ | __UNAUTHORIZED__: _Requiere autenticación_ |
| __ERRORS__ | ___404___ | __NOT FOUND__: _No encontrado (el recurso no existe)_ |
| __ERRORS__ | ___415___ | __UNSUPPORTED MEDIA TYPE__: _Media type (formato) no soportado_ |
| __ERRORS__ | ___500___ | __INTERNAL SERVER ERROR__: _Error interno_ |

## Recursos

* Usuario -> /usuario/:id_usuario?
  + Información -> /usuario/:id_usuario/info/:id_info?
  + Ubicaciónes -> /usuario/:id_usuario/ubicacion/:id_ubicacion?
  + Comentarios -> /usuario/:id_usuario/comentario/:id_comentario?
  + Estadística -> /usuario/:id_usuario/estadistica/:id_estadistica?
  + Valoración -> /usuario/:id_usuario/valoracion/:id_valoracion?
  + Productos -> /usuario/:id_usuario/producto/:id_producto?
  + Transacción -> /usuario/:id_usuario/transaccion/:id_transaccion?
* Transacción -> /transaccion/:id_transaccion?
  + Productos -> /transaccion/:id_transaccion/producto/:id_producto?
* Productos -> /producto/:id_producto?/?nombre=mango&pagina=3

### USUARIO

```javascript
{
  "email":{"type":String,"required":true,"unique":true},
  "password":{"type":String,"required":true,"unique":true},
  "creado":{"type":Date,"default":Date.now}
}
```

#### INFORMACIÓN
```javascript
{
  "nombre":{"type":String,"required":true},
  "apellido":{
    "paterno":{"type":String,"required":true},
    "materno":{"type":String,"required":true}
  },
  "telefono":[
    {
      "numero":{"type":Integer,"required":true}
    }
  ],
  "sexo":{"type":String},
  "nacimiento":{"type":Date,"required":true}
}
```

* /usuario/:id_usuario/info/:id_info?
  - POST -> /usuario/:id_usuario/info/
  - GET -> /usuario/:id_usuario/info/:id_info
  - PUT -> /usuario/:id_usuario/info/:id_info
  - DELETE -> /usuario/:id_usuario/info/:id_info

* POST

SOLICITUD /usuario/:id_usuario/info/

```JSON
{
  "nombre":"Joaquin",
  "apellido":{
    "paterno":"Garcia",
    "materno":"S"
  },
  "telefono":[
    {
      "numero":"58238072"
    }
  ],
  "sexo":"masculino",
  "nacimiento":"1992-07-22"
}
```

RESPUESTA

```JSON
{
  "status":201,
  "mensaje":"Recurso creado con exito",
  "body":{
    "id":"a18654f0c23a982",
    "nombre":"Joaquin",
    "apellido":{
      "paterno":"Garcia",
      "materno":"S"
    },
    "telefono":[
      {
        "numero":58238072
      }
    ],
    "sexo":"masculino",
    "nacimiento":"1992-07-22",
    "creado":"2016-05-16"
  }
}
```
#### UBICACIONES

```javascript
{
  "estado":{"type":String,"required":true},
  "municipio":{"type":String,"required":true},
  "localidad":{"type":String,"required":true},
  "cp":{"type":String,"required":true},
  "numero":{
    "interior":{"type":String,"required":true},
    "exterior":{"type":String,"required":true}
  },
  "calle":{"type":String,"required":true},
  "coordenadas":[
    {
      "latitud":{"type":String,"required":true},
      "longitud":{"type":String,"required":true}
    }
  ]
}
```
#### COMENTARIO

```javascript
{
  "usuario":{"type":Reference,"required":true},
  "comentario":{"type":String,"required":true},
  "creado":{"type":Date,"default":Date.now}
}
```

#### ESTADÍSTICA

```javascript
{
  "transaccion":{
    "compras":[],
    "ventas":[],
    "total":{"type":Integer,"required":true}
  }
}
```

#### VALORACIÓN

```javascript
{
  "usuario":{"type":Reference,"required":true},
  "valoración":{"type":Integer,"required":true}
}
```

### TRANSACCION

```javascript
{
  "usuario":{"type":Reference,"required":true},
  "creado":{"type":Date,"default":Date.now},
  "productos":[
    {
      "empaque":{"type":String,"required":true},
      "cantidad":{"type":Float,"required":true},
      "subtotal":{"type":Float,"required":true}
    }
  ],
  "total":{"type":Float,"required":true}
}
```

### PRODUCTO

```javascript
{
  "usuario":{"type":Reference,"required":true},
  "nombre":{"type":String,"required":true},
  "descripcion":{"type":String},
  "imagen":{"type":String},
  "precios":[
    {
      "unidad":{"type":String,"required":true},
      "precio":{"type":Float,"required":true}
    }
  ]
}
```
