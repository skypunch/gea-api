(function(){
  const crypto = require('crypto');
  module.exports= function(length=16){
    return crypto.randomBytes(length).toString('hex');
  }
})();
