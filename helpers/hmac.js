(function(){
  const crypto = require('crypto');
  module.exports= function(secret = 'my awesome secret!', alg = 'sha256'){
    const hmac = crypto.createHmac(alg,secret);
    return function(data){
      var dig = '';
      hmac.on('readable', () => {
        var data = hmac.read();
        if (data){
          dig = data.toString('hex');
        }
      });

      hmac.write(data);
      hmac.end();

      return dig;
    }
  }
})();
